// const fs = require("fs");

// const { toBN, randomHex } = require("web3-utils");
// const { takeSnapshot, revertSnapshot } = require("../scripts/ganacheHelper");

// const Tornado = artifacts.require("./ETHTornado.sol");
// const { ETH_AMOUNT, MERKLE_TREE_HEIGHT } = process.env;

// const websnarkUtils = require("websnark/src/utils");
// const buildGroth16 = require("websnark/src/groth16");
// const stringifyBigInts =
//   require("websnark/tools/stringifybigint").stringifyBigInts;
// const unstringifyBigInts2 =
//   require("snarkjs/src/stringifybigint").unstringifyBigInts;
const snarkjs = require("snarkjs");
const bigInt = snarkjs.bigInt;
const crypto = require("crypto");
const circomlib = require("circomlib");
const MerkleTree = require("fixed-merkle-tree");

const rbigint = (nbytes) =>
  snarkjs.bigInt.leBuff2int(crypto.randomBytes(nbytes));
const pedersenHash = (data) =>
  circomlib.babyJub.unpackPoint(circomlib.pedersenHash.hash(data))[0];
const pedersenHash_2 = (data) => circomlib.pedersenHash.hash(data);
const toFixedHex = (number, length = 32) =>
  "0x" +
  bigInt(number)
    .toString(16)
    .padStart(length * 2, "0");
const getRandomRecipient = () => rbigint(20);

BigInt.prototype["toJSON"] = function () {
  return this.toString();
};

function generateDeposit() {
  let deposit = {
    secret: rbigint(31),
    nullifier: rbigint(31),
  };
  const preimage = Buffer.concat([
    deposit.nullifier.leInt2Buff(31),
    deposit.secret.leInt2Buff(31),
  ]);
  deposit.commitment = pedersenHash(preimage);
  return deposit;
}

const tree = new MerkleTree(16);

const deposit = generateDeposit();
tree.insert(deposit.commitment);
const { pathElements, pathIndices } = tree.path(0);

const input_data = {
  root: tree.root(),
  nullifierHash: pedersenHash(deposit.nullifier.leInt2Buff(31)),
  nullifier: deposit.nullifier,

  relayer: "0x8EBb0380a0C88a743867A14409AED16eb3eC93eA",
  recipient: "768046622761304935951257164293598741076624715619",
  fee: "50000000000000000",
  refund: "100000000000000000",
  // relayer: operator,
  // recipient,
  // fee,
  // refund,
  secret: deposit.secret,
  pathElements: pathElements,
  pathIndices: pathIndices,
};

console.log(JSON.stringify(input_data));

// const nullifier = 337750441743537117259945809957681472613953802882236680664715428204316132880n;
// const nullifier_buf = nullifier.leInt2Buff(31);
// console.log({ nullifier_buf });
// const nullifier_hash = pedersenHash(nullifier.leInt2Buff(31));

// const nullifier_hash = pedersenHash(nullifier)
// console.log({ nullifier_hash });
// expected: 10700765031549737019695892226146175986360939787941694441715836142154146527645

// 8261155327115765884020574884050740727108150085542501182819917289286333485950n
