use num_bigint::BigUint;
use webb_pedersen_hash::hash as pedersen_hash;

fn main() {
    let nullifier = BigUint::parse_bytes(
        "337750441743537117259945809957681472613953802882236680664715428204316132880".as_bytes(),
        10,
    )
    .unwrap();

    let nullifier_buf = nullifier.to_bytes_le();

    // This is the output from the js code,
    // nullifier_buf is a Vec<u8> with the good values in decimal
    // let nullifier_buf = vec![
    //     0x10, 0x06, 0xef, 0x0c, 0x09, 0x2c, 0x25, 0xba, 0xf4, 0xac, 0x51, 0xcf, 0x21, 0xeb, 0xf1,
    //     0x5d, 0x71, 0x96, 0x29, 0x03, 0x15, 0xa8, 0xa7, 0x4f, 0x19, 0x6e, 0xbc, 0x28, 0xf4, 0x28,
    //     0xbf,
    // ];

    // Here it's not good !
    // expected result is wrong...

    let nullifier_hash_array = pedersen_hash(&nullifier_buf).unwrap();
    // let nullifier_hash_array = pedersen_hash(&preimage).unwrap();
    let nullifier_hash_uint = BigUint::from_bytes_le(&nullifier_hash_array);
    let nullifier_hash = BigUint::to_string(&nullifier_hash_uint);

    println!("nullifier_hash_uint: {:?}", nullifier_hash_uint);
    println!("nullifier_hash: {:?}", nullifier_hash);
    println!(
        "expected: '10700765031549737019695892226146175986360939787941694441715836142154146527645'"
    );
}
